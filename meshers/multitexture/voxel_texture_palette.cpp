#include "voxel_texture_palette.h"
#include "../../util/span.h"

namespace zylann::voxel {

VoxelTexturePalette::VoxelTexturePalette() {
}

void VoxelTexturePalette::set_color(int texture_index, int color_index, Color color) {
	ERR_FAIL_INDEX(texture_index, static_cast<int>(_textures.size()));
	_textures[texture_index].set_color(color_index, color);
}

Color VoxelTexturePalette::get_color(int texture_index, int color_index) const {
	ERR_FAIL_INDEX_V(texture_index, static_cast<int>(_textures.size()), Color());
	return _textures[texture_index].get_color(color_index);
}

PackedColorArray VoxelTexturePalette::get_textures() const {
	PackedColorArray dst;
	// Not resizing up-front to make code portable, because in GDExtension writing to packed arrays has different
	// syntax.
	for (unsigned int i = 0; i < _textures.size(); ++i) {
            for (unsigned int j = 0; j < _textures[i].size(); ++j) {
		dst.push_back(_textures[i].get_color(j));
            }
	}
	return dst;
}

void VoxelTexturePalette::set_textures(PackedColorArray textures) {
	// Color count is fixed, but we can't easily prevent Godot from allowing users to set a dynamic array
        VoxelColorPalette texture = VoxelColorPalette();
	ERR_FAIL_COND(textures.size() != static_cast<int>(_textures.size() * texture.size()));
	for (unsigned int i = 0; i < _textures.size(); ++i) {
            for (unsigned int j = 0; j < _textures[i].size(); ++j) {
                _textures[i].set_color(j, Color8(textures[i]));
            }
	}
}

void VoxelTexturePalette::clear() {
	for (size_t i = 0; i < _textures.size(); ++i) {
		_textures[i].clear();
	}
}

PackedInt32Array VoxelTexturePalette::_b_get_data() const {
	PackedInt32Array textures;
	// Not resizing up-front to make code portable, because in GDExtension writing to packed arrays has different
	// syntax.
	for (size_t i = 0; i < _textures.size(); ++i) {
                for (unsigned int j = 0; j < _textures[i].size(); ++j) {
                        textures.push_back(((Color8) _textures[i].get_color(j)).to_u32());
                }
	}
	return textures;
}

void VoxelTexturePalette::_b_set_data(PackedInt32Array colors) {
        VoxelColorPalette texture = VoxelColorPalette();
	ERR_FAIL_COND(colors.size() > static_cast<int>(_textures.size() * texture.size()));
	for (unsigned int i = 0; i < _textures.size(); ++i) {
                for (unsigned int j = 0; j < _textures[i].size(); ++j) {
                        _textures[i].set_color(j, Color8::from_u32(colors[i * texture.size() + j]));
                }
	}
}

void VoxelTexturePalette::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_color", "texture_index", "color_index", "color"), &VoxelTexturePalette::set_color);
	ClassDB::bind_method(D_METHOD("get_color", "texture_index", "color_index"), &VoxelTexturePalette::get_color);

	//ClassDB::bind_method(D_METHOD("set_texture", "index", "texture"), &VoxelTexturePalette::set_texture);
	//ClassDB::bind_method(D_METHOD("get_texture", "index"), &VoxelTexturePalette::get_texture);

	ClassDB::bind_method(D_METHOD("set_textures", "textures"), &VoxelTexturePalette::set_textures);
	ClassDB::bind_method(D_METHOD("get_textures"), &VoxelTexturePalette::get_textures);

	ClassDB::bind_method(D_METHOD("set_data", "d"), &VoxelTexturePalette::_b_set_data);
	ClassDB::bind_method(D_METHOD("get_data"), &VoxelTexturePalette::_b_get_data);

	// This is just to allow editing colors in the editor
	ADD_PROPERTY(PropertyInfo(Variant::PACKED_COLOR_ARRAY, "textures", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_EDITOR),
			"set_textures", "get_textures");

	ADD_PROPERTY(PropertyInfo(Variant::PACKED_INT32_ARRAY, "data", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_STORAGE),
			"set_data", "get_data");

	BIND_CONSTANT(MAX_TEXTURES);
}

} // namespace zylann::voxel
