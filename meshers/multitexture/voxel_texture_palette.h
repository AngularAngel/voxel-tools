#ifndef VOXEL_TEXTURE_PALETTE_H
#define VOXEL_TEXTURE_PALETTE_H

#include "../../util/fixed_array.h"
#include "../../util/godot/classes/resource.h"
#include "../../util/math/color8.h"
#include "../cubes/voxel_color_palette.h"

namespace zylann::voxel {

// Associates small numbers to colors, so colored voxels can be specified using less memory.
class VoxelTexturePalette : public Resource {
	GDCLASS(VoxelTexturePalette, Resource)
public:
	static const unsigned int MAX_TEXTURES = 256;

	VoxelTexturePalette();

	void set_color(int texture_index, int color_index, Color color);
	Color get_color(int texture_index, int color_index) const;

	PackedColorArray get_textures() const;
	void set_textures(PackedColorArray colors);

	void clear();

	// Internal

	inline void set_texture(uint8_t i, VoxelColorPalette t) {
		_textures[i].set_colors(t.get_colors());
	}

	inline VoxelColorPalette get_texture(uint8_t i) const {
		return _textures[i];
	}

	inline unsigned int size() const {
		return _textures.size();
	}

private:
	PackedInt32Array _b_get_data() const;
	void _b_set_data(PackedInt32Array colors);

	static void _bind_methods();

	FixedArray<VoxelColorPalette, MAX_TEXTURES> _textures;
};

} // namespace zylann::voxel

#endif // VOXEL_TEXTURE_PALETTE_H
